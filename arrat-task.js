let arr = [50,60,60,45,71]

let odd = arr.filter(num =>{
    return num%2 !== 0;
})
console.log(odd);

let even = arr.filter(num => {
    return num%2 == 0;
})
console.log(even)

let oddSum = 0;
let evenSum = 0;
  
    // Calculation the sum using forEach
    odd.forEach(x => {
        oddSum += x;
    });
    console.log("Sum of ODD = " + oddSum);    
    even.forEach(x => {
        evenSum += x;
    });
    console.log("Sum of EVEN = " + evenSum); 