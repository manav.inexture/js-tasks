/* "Given an array arr, find element pairs whose sum equal the second argument arg and return the sum of their indices.

You may use multiple pairs that have the same numeric elements but different indices. Each pair should use the lowest possible available indices. Once an element has been
 used it cannot be reused to pair with another element.

For example pairwise([7, 9, 11, 13, 15], 20) returns 6. The pairs that sum to 20 are [7, 13] and [9, 11]. 
Answer should be total of indexes
We can then write out the array with their indices and values."*/

function challangeArr(arr,target)
{
    sol=[];
    for(let i=0;i<arr.length;i++)
    {
        j=i+1;
        for(let j=i+1;j<arr.length;j++)
        {
            if((arr[i]+arr[j]) == target && arr[i]<=target && arr[j]<=target)
            {
                sol.push(i);
                sol.push(j);
            }
        }
    }
    if(sol.length!==0)
    {
        sum=0;
        sol.forEach(i => {
            sum+=i;
        });
        console.log("index are:" +sol + " ans: " + sum);
    }
    else
    console.log("No solution found");
}
challangeArr([7, 9, 11, 13, 15], 20);